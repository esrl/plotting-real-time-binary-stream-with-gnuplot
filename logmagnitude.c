// Compile with: gcc -Wall -O3 -lfftw3f -lm -o logmagnitude logmagnitude.c

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <assert.h>
#include <math.h>

#include <fftw3.h>

int main(int argc, char **argv) {
  int nbits    = 11;        // 2048 window size

  if( argc > 1 ) {
    nbits = atoi(argv[1]);
  }

  int fft_size = 1<<nbits;
  float         *bufin  = malloc( fft_size * sizeof(float) );
  fftwf_complex *bufout = fftwf_alloc_complex(fft_size / 2 + 1);
  float         *out    = malloc( (fft_size/2+1) * sizeof(float) );
  bool time_to_quit     = false;
  bool apply_window     = true;

  assert(bufin );
  assert(bufout);
  assert(out   );

  fftwf_plan p = fftwf_plan_dft_r2c_1d(fft_size, bufin, bufout, FFTW_ESTIMATE);

  size_t n;
  while(1) {
    n = fread(bufin, sizeof(float), fft_size, stdin);
    if( n != fft_size ) {        // we probably reached end of input, pad the bufin with zeros and signal quit
      if( n == 0 )
        break;
      fprintf(stderr, "fft: Padding!\n");
      for( ; n < fft_size; n++ )
        bufin[n] = 0;
      time_to_quit = true;
    }

    // Apply a Hann window to the input
    if( apply_window )
      for(int i=0; i<fft_size; i++)
        bufin[i] *= 0.5 * (1 - cos( 2 * M_PI * i / (fft_size - 1) ) );

    fftwf_execute_dft_r2c(p, bufin, bufout);

    out[0] = 0.0;
    for(int i=1; i<fft_size/2; i++)
      out[i] = 20 * logf(sqrt( powf( bufout[i][0], 2.0 ) + powf( bufout[i][1], 2.0 ) ) / fft_size );

    fwrite(out, sizeof(float), fft_size/2+1, stdout);
    if(time_to_quit || feof(stdin))
      break;
  }

  fftwf_destroy_plan(p);

  return 0;
}

#!/bin/sh                                                                                                     

trap cleanup INT

cleanup() {
	echo "Cleaning up ..."
	rm -f $data_in $cmd_in
	kill $gp_pid
	exit
}

# Hardcoded stream sample rate
sr=${1-44100}

# Named pipes for receiving data and commands
data_in=$XDG_RUNTIME_DIR/spectrum_data.fifo
cmd_in=$XDG_RUNTIME_DIR/spectrum_cmd.fifo

rm -f $data_in $cmd_in
mkfifo $data_in $cmd_in

gnuplot < $cmd_in &
gp_pid=$!

# Find nearest sr/10 fft_window size so we have ca 10 plots per second
# Adapted from https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
fft_window=$(( sr/10 ))
fft_window=$(( fft_window - 1 ))
fft_window=$(( fft_window | (fft_window >>  1) ))
fft_window=$(( fft_window | (fft_window >>  2) ))
fft_window=$(( fft_window | (fft_window >>  4) ))
fft_window=$(( fft_window | (fft_window >>  8) ))
fft_window=$(( fft_window | (fft_window >> 16) ))
fft_window=$(( fft_window + 1 >> 1 ))

# The fft program expects the power of 2 i.e. 2**nbits
nbits=0
v=$fft_window
while [ $(( $v >> 1 )) -gt 0 ]; do
	v=$(( v >> 1 ))
	nbits=$(( nbits + 1 ))
done

# The magnitude output of fft is half the fft window size plus one
fft_plot_size=$(( fft_window / 2 + 1 ))

# Necessary writer and reader of the named pipes
exec 3>$cmd_in
exec 4<$data_in &

echo "set term wxt noraise"     > $cmd_in
echo "set xrange [0:$((sr/2))]" > $cmd_in      # sr/10 because 10 plots per second 
echo "set yrange [-300:0]"      > $cmd_in
echo "unset key"                > $cmd_in

# Again sr/10 for 10 chunks per second. Multiplied by 2 because samples are 2 bytes long
./logmagnitude $nbits |
SHELL=/bin/sh split -b $(( 4 * fft_plot_size )) \
--filter="cat > $data_in; echo 'plot \"$data_in\" binary format=\"%float32\" using (\$0 * ($sr/2) / $fft_plot_size):1 with lines' > $cmd_in"

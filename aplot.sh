#!/bin/sh                                                                                                     

trap cleanup INT

cleanup() {
	echo "Cleaning up ..."
	rm -f $data_in $cmd_in
	kill $gp_pid
	exit
}

# Named pipes for receiving data and commands
data_in=$XDG_RUNTIME_DIR/aplot_data.fifo
cmd_in=$XDG_RUNTIME_DIR/aplot_cmd.fifo

# Hardcoded stream sample rate
sr=${1-44100}

rm -f $data_in $cmd_in
mkfifo $data_in $cmd_in

gnuplot < $cmd_in &
gp_pid=$!

# Necessary writer and reader of the named pipes
exec 3>$cmd_in
exec 4<$data_in &

echo "set term wxt noraise"      > $cmd_in
echo "set xrange [0:$((sr/10))]" > $cmd_in      # sr/10 because 10 plots per second 
echo "set yrange [-33000:33000]" > $cmd_in
echo "unset key"                 > $cmd_in

# Again sr/10 for 10 chunks per second. Multiplied by 2 because samples are 2 bytes long
SHELL=/bin/sh split -b $(( 2 * $sr / 10 )) \
	--filter="cat > $data_in; echo 'plot \"$data_in\" binary format=\"%int16\" using 0:1 with lines' > $cmd_in"

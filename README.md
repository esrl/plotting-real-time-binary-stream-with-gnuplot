# Plotting binary real-time stream with Gnuplot


# Table of Contents
1. [Introduction](#introduction)
2. [Some theory](#some-theory)
3. [A few notes about remote controlling Gnuplot](#a-few-notes-about-remote-controlling-gnuplot)
4. [Example plotting a live microphone stream](#example-plotting-a-live-microphone-stream)
5. [Example plotting a live microphone stream without split](#example-plotting-a-live-microphone-stream-without-split)
6. [Example plotting the spectrum of a live microphone stream](#example-plotting-the-spectrum-of-a-live-microphone-stream)

## Introduction
Ever since I learnt about Gnuplot I have been experimenting with real-time plotting. This has usually produced acceptable results when dealing with low-bandwidth text data. However, when dealing with high-bandwidth binary streams, such as audio, it is another matter. The solution to this has eluded me for some time.

## Some theory
In order for Gnuplot to plot a stream, some stream chunking is needed with some well-timed plot commands:
```mermaid
graph LR
    subgraph Stream of Data
    id1(".............")
    end
    
    subgraph Chunks of Data 
    Chunk1 --> Chunk2 
    Chunk2 --> Chunk3
    Chunk3 --> ...
    end

    id1 --> Chunk1
```
Luckily there is a command in [GNU coreutils](https://www.gnu.org/software/coreutils) that allows this already, i.e. [split](https://www.gnu.org/software/coreutils/manual/html_node/split-invocation.html). For example for line-based data you could insert foo every 2 lines like this:
```
seq inf | split -l2 --filter='cat; echo foo' | head
```
Output:
```
1
2
foo
3
4
foo
5
6
foo
7
```
As the example shows, split will "chunk" up the data and pass it to a filter script on standard input. If this input is passed to Gnuplot accompanied by an appropriate plot command, we should have our desired result.


## A few notes about remote controlling Gnuplot
There are several ways of achieving this, e.g.:

 - Run Gnuplot in a `tmux` session and send commands with `send-keys`.
 - Have Gnuplot take commands from a unix, udp or tcp socket with `socat`.
 - Using the `coproc` feature of various shells.
 - Use a named pipe.

This example uses the last alternative as it fits best with passing the data-chunk in through a separate named pipe.

Note that the named pipe attached to Gnuplot always needs at least one writer, otherwise it will be closed and Gnuplot will exit. The reverse is true for the output named pipe, it always needs a reader.

I have yet to find an adequate explanation of why these writers/readers are necessary.


## Example plotting a live microphone stream
The following example assumes you have saved the attached script to `aplot.sh`.

On my system the samples coming from my microphone are encoded like this:
```
pacmd list-sources | 
awk '
/^ *\* index/ { f=1 }
f==1 && /sample spec:/ { print $3, $4, $5; exit }
'
```
Output:
```
s16le 2ch 44100Hz
```
Which means signed 16 bit integers, 2 interleaved channels at 44100 samples per second.

In order to simplify the plotting, I chose to remove one of the channels with [sox](http://sox.sourceforge.net/) like this:
```
sox -qD -c2 -r44100 -t s16 - -t s16 - remix 1
```
Here the `-q` means quiet, the `-D` omits adding dithering, the `-c`, `-r` and `-t` options set the input stream parameters.  The last `-t s16` sets the encoding of the output, `remix 1` chooses channel one, i.e. removes channel 2.

The stream can now be plotted like this:
```
# Pulseaudio pipe default source to standard out and ignore broken pipe warnings
parec --latency-msec=1 2> /dev/null           |

# Remove the second channel
sox -qD -c2 -r44100 -t s16 - -t s16 - remix 1 |

# Leave the rest to split and Gnuplot
/path/to/aplot.sh
```
Use <kbd>Ctrl</kbd>-<kbd>c</kbd> to exit.

Below is a screenshot of the output.

![Screenshot of an A-plot](https://i.imgur.com/YX1wF9q.png)


## Example plotting a live microphone stream without split
Here is another method that uses `head` instead of `split`, although I don't find it as convenient.

In one terminal create a fifo and add a permanent reader:
```
mkfifo /tmp/aplot.fifo
exec 3</tmp/aplot.fifo
```
In another terminal run Gnuplot on the following script (adapted to the parameters in the previous example):
```gnuplot
set term wxt noraise
plot [0:4410] [-33000:33000] '<head -c8820 /tmp/aplot.fifo' binary format='%int16' using 0:1 with lines
reread
```
Now start the input stream:
```
parec --latency-msec=1 2> /dev/null |
sox -qD -c2 -r44100 -t s16 - -t s16 - remix 1 > /tmp/aplot.fifo
```


## Example plotting the spectrum of a live microphone stream
With a few additions it is possible to use the same technique to plot the log magnitude of the same input.  I wrote a small wrapper for `libfftw` which takes single channel single-precision floats on its standard input and outputs the log magnitude on standard output, this program can be found in `logmagnitude.c`.

If we modify the A-plot example above to use single-precision floats, we can now do:
```
parec --latency-msec=1 2> /dev/null |
sox -qD -c2 -r44100 -t s16 - -t f32 - remix 1 |
./spectrum.sh
```
You need to modify the `spectrum.sh` script to use the correct path for the `logmagnitude` program.

Below is a screenshot taken while this sweep was being played:
```
sox -q -n -d synth 10 sine 22k+50 gain -12
```
![Screenshot of signal log magnitude with a peak at ca. 13kHz](https://i.imgur.com/lGuaMn4.png)
